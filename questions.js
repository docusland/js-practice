let selectElementsStartingWithA = function(array) {
        // Cette fonction ne doit retourner que les éléments commençant par la lettre A
}

var selectElementsStartingWithVowel = function (array) {
    // Cette fonction ne doit retourner que les éléments commençant par une voyelle
}

var removeNullElements = function (array) {
    // Cette fonction doit retirer du tableau tous les éléments null
    return "write your method here!";
}

var removeNullAndFalseElements = function (array) {
    // Cette fonction doit retirer du tableau tous les éléments null et false
    return "write your method here!";
}

var reverseWordsInArray = function (array) {
    // Inverser l'ensemble des mots d'un tableau
    return "write your method here!";
}

var everyPossiblePair = function (array) {
    // Réaliser l'ensemble des appariements possibles.
    return "write your method here!";
}

var allElementsExceptFirstThree = function (array) {
    // Retourner le tableau en retirant les trois premiers éléments.
    return "write your method here!";
}

var addElementToBeginning = function (array, element) {
    // Ajoutez l'élement en tête de tableau.
    return 'Write your method here';
}

var sortByLastLetter = function (array) {
    // Triez le tableau en fonction de la dernière lettre de chaque élément.
    return 'Write your method here';
}

var getFirstHalf = function (string) {
    // Retourner la première moitié du mot
    return 'Write your method here';
}

var makeNegative = function (number) {
    // Faites en sorte que la valeur retournée soit négative.
    return 'Write your method here';
}

var numberOfPalindromes = function (array) {
    // Combien de mots sont des palindromes 
    // Un palindrome est un mot miroir (exemple : laval ou encore radar. )
    return 'Write your method here';
}

var shortestWord = function (array) {
    // Cette fonction retourne le plus petit mot
    return 'Write your method here';
}

var longestWord = function (array) {
    // Cette fonction retourne le plus grand mot
    return 'Write your method here';
}

var sumNumbers = function (array) {
    // Cette fonction retourne la somme des valeurs contenues dans le tableau
    return 'Write your method here';
}

var repeatElements = function (array) {
    // Cette fonction dédouble un tableau
    return 'Write your method here';
}

var stringToNumber = function (string) {
    // Convertissez un texte en nombre
    return 'Write your method here';
}

var calculateAverage = function (array) {
    // Cette fonction calcule la moyenne des valeurs contenues dans le tableau
    return 'Write your method here';
}

var getElementsUntilGreaterThanFive = function (array) {
    // Cette fonction retourne les premières valeurs d'un tableau, jusqu'à rencontrer un valeur supérieure à 5
    return 'Write your method here';
}

var convertArrayToObject = function (array) {
    // Cette fonction convertit un tableau en objet
    // ['un', 'deux', 'trois', 'quatre'] deviendra {'un': 'deux', 'trois': 'quatre'}
    return 'Write your method here';
}

var getAllLetters = function (array) {
    // Cette fonction retourne un tableau de lettres, listées en ordre alphabétique.
    // Les lettres listées sont celles des mots recensés dans le tableau donné en paramètre de fonction
    return 'Write your method here';
}

var swapKeysAndValues = function (object) {
    // Cette fonction prend un objet en paramètre et inverse les clés avec les valeurs
    return 'Write your method here';
}

var sumKeysAndValues = function (object) {
    // Un objet est passé en paramètre, il contient des entiers en clé et en valeurs
    // Le but est de réaliser la somme de toutes ces clés et valeurs
    return 'Write your method here';
}

var removeCapitals = function (string) {
    // Cette fonction retire toutes les majuscules
    return 'Write your method here';
}

var roundUp = function (number) {
    // Cette fonction arrondi à l'entier supérieur.
    return 'Write your method here';
}

var formatDateNicely = function (date) {
    // Cette fonction formate une date au format dd/mm/YYYY
    return 'Write your method here';
}

var getDomainName = function (string) {
    // Cette fonction retourne le nom du domaine d'une url ou d'un email 
    // Attention, il n'est pas attendu à ce que le TLD soit retourné.
    return 'Write your method here';
}

var titleize = function (string) {
    // Cette fonction retourne la string, avec chaque mot ayant la première lettre en majuscule et le reste en minuscule. 
    // Cette fonction laisse en minuscule les mots and et the s'ils ne sont pas en prime position.
    return 'Write your method here';
}

var checkForSpecialCharacters = function (string) {
    // Cette fonction vérifie si un texte contient des caractères spéciaux.
    return 'Write your method here';
}

var squareRoot = function (number) {
    // Cette fonction calcule la racine carrée d'un nombre.
    return 'Write your method here';
}

var factorial = function (number) {
    // Cette fonction calcule la factorielle d'un nombre. 
    // Ex : 5 ! = 5 x 4 x 3 x 2 x 1 = 120  
    return 'Write your method here';
}

var findAnagrams = function (string) {
    // Cette fonction génère l'ensemble des anagrammes possibles d'un mot. 
    // Pour rappel chien a pour anagramme niche ou encore chine. 
    // Cette fonction doit retourner l'ensemble des possibilités, listés en ordre alphabétique.
    return 'Write your method here';
}

var convertToCelsius = function (number) {
    // Conversion d'une température en Farenheit vers une température en Celsius. 
    // Rappel de la formule : F° = C°*1.8 + 32   //   C° = (F° - 32) / 1.8
    return 'Write your method here';
}

var letterPosition = function (array) {
    // Cette fonction prends un tableau de lettres en paramètres
    // et retourne un tableau ayant leur position dans l'alphabet
    // ex : ['A', 'b'] => [1, 2]
    return 'Write your method here';
}
